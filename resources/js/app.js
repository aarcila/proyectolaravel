require('./bootstrap');

import { createApp } from "vue";

import router from './router'
import ClientesIndex from './components/clientes/ClientesIndex.vue'

createApp({
    components: {
        ClientesIndex
    }
}).use(router).mount('#app')

