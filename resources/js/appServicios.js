require('./bootstrap');

import { createApp } from "vue";

import router from './router'
import ServiciosIndex from './components/clientes/ServiciosIndex.vue'

createApp({
    components: {
        ServiciosIndex
    }
}).use(router).mount('#appServicios')