import { ref } from 'vue';
import axios from "axios";

import { useRouter } from 'vue-router';

export default function useClientes(){
    const errors = ref('')
    const router = useRouter()
    const clientes = ref([])
    const cliente = ref([])
    
    const getClientes = async() => {
        let response = await axios.get('/public/api/clientes')
        clientes.value = response.data

        // .then(response => {
        //     clientes.value = response.data;
        // })
       
    }

    const getCliente = async(id) => {
        let response = await axios.get('/public/api/clientes/' + id)
        cliente.value = response.data.data
    }

    const storeCliente = async(data) => {
        errors.value = ''
        try{
            await axios.post('/public/api/clientes', data)
            await router.push({name: 'clientes.index'})
        }catch(e){
            if(e.response.status === 422){
                errors.value = e.response.data.errors
            }
            
        }
    }

    const updateCliente = async(id) => {
        errors.value = ''
        try{
            await axios.put('/public/api/clientes/' + id, cliente.value)
            await router.push({name: 'clientes.index'})
        }catch(e){
            if(e.response.status === 422){
                errors.value = e.response.data.errors
            }
            
        }
    }


    const destroyCliente = async (id) => {
        await axios.delete('/public/api/clientes/' + id)
    }


    return {
        errors,
        clientes,
        cliente,
        getClientes,
        getCliente,
        storeCliente,
        updateCliente,
        destroyCliente
    }

}