import { ref } from 'vue';
import axios from "axios";

import { useRouter } from 'vue-router';

export default function useServicios(){
    const errors = ref('')
    const router = useRouter()
    const servicios = ref([])
    const servicio = ref([])
    
    const getServicios = async() => {
        let response = await axios.get('/public/api/servicios')
        servicios.value = response.data
       
    }

    const getServicio = async(id) => {
        let response = await axios.get('/public/api/servicios/' + id)
        servicio.value = response.data.data
    }

    const storeServicio = async(data) => {
        errors.value = ''
        try{
            await axios.post('/public/api/servicios', data)
            await router.push({name: 'servicios.index'})
        }catch(e){
            if(e.response.status === 422){
                errors.value = e.response.data.errors
            }
            
        }
    }

    const updateServicio = async(id) => {
        errors.value = ''
        try{
            await axios.put('/public/api/servicios/' + id, servicio.value)
            await router.push({name: 'servicios.index'})
        }catch(e){
            if(e.response.status === 422){
                errors.value = e.response.data.errors
            }
            
        }
    }


    const destroyServicio = async (id) => {
        await axios.delete('/public/api/servicios/' + id)
    }


    return {
        errors,
        servicios,
        servicio,
        getServicios,
        getServicio,
        storeServicio,
        updateServicio,
        destroyServicio
    }

}