import { createRouter, createWebHistory } from "vue-router";

import ClientesIndex from '../components/clientes/ClientesIndex.vue'
import ClientesCreate from '../components/clientes/ClientesCreate'
import ClientesEdit from '../components/clientes/ClientesEdit'
import ClientesVer from '../components/clientes/ClientesVer'
import ServiciosIndex from '../components/clientes/ServiciosIndex.vue'
import ServiciosCreate from '../components/clientes/ServiciosCreate'
import ServiciosEdit from '../components/clientes/ServiciosEdit'
import ServiciosVer from '../components/clientes/ServiciosVer'

const routes = [
    {
        path: '/public/home/',
        name: 'clientes.index',
        component: ClientesIndex
    },
    {
        path: '/public/clientes/create',
        name: 'clientes.create',
        component: ClientesCreate
    },
    {
        path: '/public/clientes/:id/edit',
        name: 'clientes.edit',
        component: ClientesEdit,
        props: true
    },
    {
        path: '/public/clientes/:id/ver',
        name: 'clientes.ver',
        component: ClientesVer,
        props: true
    },
    
    {
        path: '/public/service/',
        name: 'servicios.index',
        component: ServiciosIndex
    },
    {
        path: '/public/servicios/create',
        name: 'servicios.create',
        component: ServiciosCreate
    },
    {
        path: '/public/servicios/:id/edit',
        name: 'servicios.edit',
        component: ServiciosEdit,
        props: true
    },
    {
        path: '/public/servicios/:id/ver',
        name: 'servicios.ver',
        component: ServiciosVer,
        props: true
    }

]

export default createRouter({
    history: createWebHistory(),
    routes
})
