@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12" id="app">
            <div class="card">
                <div class="card-header"><a class="nav-link" href="{{ url('/service') }}">{{ __('Ir a servicios') }}</a></div>
                <div class="card-header"><a class="nav-link" href="{{ url('/home') }}">{{ __('Ir a clientes') }}</a></div>
                            
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <router-view />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection