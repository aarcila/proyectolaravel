@extends('layouts.app')

@section('content')
<div class="container">
        <div class="col-md-12" id="appServicios">
            <div class="card">
                <div class="card-header">{{ __('Servicios') }}</div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <router-view />
                </div>
            </div>
        </div>
    </div>
</div>
@endsection